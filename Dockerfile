# Builder image
# Base image
FROM maven:3.5-jdk-8 as builder

# Copy source files
COPY . /home/root/build/

# Move to workdir
WORKDIR /home/root/build/

# Run mvn build
RUN mvn package -DskipTests

# Executer image
FROM openjdk:8-jre-alpine

ENV SPRING_CONFIG_LOCATION=file:./config/shibboleth-discovery/

COPY --from=builder /home/root/build/target/*.jar \
/srv/shibboleth-discovery/app.jar

WORKDIR /srv/shibboleth-discovery/

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "app.jar", "--spring.config.location=$SPRING_CONFIG_LOCATION"]
