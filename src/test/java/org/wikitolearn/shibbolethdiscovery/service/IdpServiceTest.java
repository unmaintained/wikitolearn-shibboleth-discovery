package org.wikitolearn.shibbolethdiscovery.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit4.SpringRunner;
import org.w3c.dom.Document;
import org.wikitolearn.shibbolethdiscovery.config.ApplicationProperties;
import org.wikitolearn.shibbolethdiscovery.entity.EntityDescriptor;
import org.xml.sax.SAXException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class IdpServiceTest {
  private Document document;

  private Map<String, EntityDescriptor> entities;

  @Autowired
  private IdpService idpService;

  @Autowired
  private ApplicationProperties applicationProperties;

  @Before
  public void setup() throws ParserConfigurationException, SAXException, IOException, ParseException {
    entities = new HashMap<>();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    Resource idemTestWayfFile = new ClassPathResource("idem_test_wayf.xml");
          DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    document = builder.parse(idemTestWayfFile.getFile());
    document.getDocumentElement().normalize();

    EntityDescriptor entity1 = new EntityDescriptor();
    entity1.setEntityId("https://grouper-jra.idem.garr.it/idp/shibboleth");
    entity1.setAlias(applicationProperties.getIdp().getAliasPrefix()
        + DigestUtils.md5Hex(entity1.getEntityId()).toUpperCase());
    entity1.setName("Grouper AA");
    entity1.setDescription("Grouper Attribute Authority per il rilascio di \"isMemberOf\"");
    entity1.setLogo("https://grouper-jra.idem.garr.it/grouper_logo_32x32.png");
    entity1.setCertificate(
        "MIIDTDCCAjSgAwIBAgIVAJP10pVJSn3Kx3V0LXDUeYcCIL0HMA0GCSqGSIb3DQEBBQUAMCMxITA"
        + "fBgNVBAMTGGdyb3VwZXItanJhLmlkZW0uZ2Fyci5pdDAeFw0xNTAzMTExMDI0MzlaFw0zNTAz"
        + "MTExMDI0MzlaMCMxITAfBgNVBAMTGGdyb3VwZXItanJhLmlkZW0uZ2Fyci5pdDCCASIwDQYJK"
        + "oZIhvcNAQEBBQADggEPADCCAQoCggEBAOodvAxWkyPkRGFCWHlrVi2G6EsJrjruXdYLecU0Pe"
        + "5+o9Ou2gOIeIHDMGcfqD32rgi+gNpLZZHifm7t/UrND9TQGXpRcFf5udd8yPW5S5bm0KWaeES"
        + "Yh/EMQAesndflx2Y4hdflp6PrPQi2+IanwlxyMyGkLgisJ6pHtP1vvAgiACSrTa2W6Eo3OBy3"
        + "45FsN+fYI4+9x4WFzcQN0C8of0j0KQJkKW5+XumvdVRGcGKk6ql2ytKHmFm8qJjEkJIV9/eCs"
        + "qx3bMzbz5UhexLJb3dao9/UbPpjmeBZjCnG/1M+dsxYY/7PmxF79LUF3/2H0glmfc21ZI331+"
        + "WjEhc/uUMCAwEAAaN3MHUwVAYDVR0RBE0wS4IYZ3JvdXBlci1qcmEuaWRlbS5nYXJyLml0hi9"
        + "odHRwczovL2dyb3VwZXItanJhLmlkZW0uZ2Fyci5pdC9pZHAvc2hpYmJvbGV0aDAdBgNVHQ4E"
        + "FgQUR7H+irL/LiOXzdNqMHLoG1x3/2EwDQYJKoZIhvcNAQEFBQADggEBAAe2/tEvGS2YXuO4Y"
        + "ShEqjKHFpuO+2bX8A1Z10MZWE9ZsPJyRpDRuanKEedFs5RlWTK+9H2oQwiBmNyBBBPZAZbnRm"
        + "CX2fOrPQq2NsO573WnYGY4vR4ZlowjmqUN5F0c97bjGQdX9dnAdASHZ9ZgF/DsP962iV80Uan"
        + "Bn3xqQrrD+Jm6xVf+I88UtrMDlVEeLYEm/wzuEvHHHB3wUSuICeWgWJc3WeE7lsLz/FeQbSr6"
        + "hpbswFgHBsgO9B6coUO8fCGpwdhTUNUBo6TO5t8pOfQegS8ftK1edZQI1uJy9GIs532aQSOFK"
        + "7HXbyrt6Huj4vUh85OCMiV7bkfDzVD2MjQ=");
    entity1.setSsoURL("https://grouper-jra.idem.garr.it/idp/profile/SAML2/POST/SSO");
    entity1.setLogoutURL("https://grouper-jra.idem.garr.it/idp/profile/SAML2/POST/SLO");
    entity1.setNameIdFormat("urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified");
          EntityDescriptor entity2 = new EntityDescriptor();
    entity2.setEntityId("https://idp.enea.it/idp/shibboleth");
    entity2.setAlias(applicationProperties.getIdp().getAliasPrefix()
        + DigestUtils.md5Hex(entity2.getEntityId()).toUpperCase());
    entity2.setRegistrationDate(sdf.parse("2015-03-17T08:32:06Z"));
    entity2.setName("ENEA Agenzia nazionale per le nuove tecnologie, l'energia e lo sviluppo economico sostenibile");
    entity2.setDescription("Identity Provider of ENEA Agenzia nazionale per le nuove tecnologie, l'energia e lo sviluppo economico sostenibile");
    entity2.setLogo("https://idp.enea.it/idp/images/institutionLogo-160x120_it.png");
    entity2.setCertificate(
        "MIIDGDCCAgCgAwIBAgIVAMxCwXCBdoqj0ieDxa5LhT4GeMq+MA0GCSqGSIb3DQEB" +
        "BQUAMBYxFDASBgNVBAMTC2lkcC5lbmVhLml0MB4XDTE0MDkyNDE0MDcyN1oXDTM0" +
        "MDkyNDE0MDcyN1owFjEUMBIGA1UEAxMLaWRwLmVuZWEuaXQwggEiMA0GCSqGSIb3" +
        "DQEBAQUAA4IBDwAwggEKAoIBAQCY6gV4PFoosigOKiGaHKJd2orroLYEbZrGtign" +
        "9VjEp0lQvAU1nipkIhJjjT3vYsB74G63KVcSdgjSOc3AWYo3KFGflZlaOnkIH1LA" +
        "/NM+4a6AxME9yOabaQnJyAmbx4CEk1hJ3UtvNBQ0hycOqzupetXpiuj5j1s12XiZ" +
        "fwnfjYzl7yqysvIJb29D8G67rnJAAuHLkPkV6IDkMjM+BMNZNB2f2g1Q5RHg4qY5" +
        "Dv4rJ5pD7SwU8JzHYgL0+qULeVdDUFbWI+u0NYhQV/VECkDbIo08LPv8gLh4daMJ" +
        "dflrR23Tbu+1p4l1ZcYXvwBEzQO2godcLSu05kLQwCqVsUBRAgMBAAGjXTBbMDoG" +
        "A1UdEQQzMDGCC2lkcC5lbmVhLml0hiJodHRwczovL2lkcC5lbmVhLml0L2lkcC9z" +
        "aGliYm9sZXRoMB0GA1UdDgQWBBT8mLuFjuqkjoSYtVVJqq60dOLYFTANBgkqhkiG" +
        "9w0BAQUFAAOCAQEANLqpt3OSB20MESmfP4Yxh/MCVXyTQAXDF1J23IvIwi/FEV/n" +
        "FpO2NzXzgNz/9RG4BGElM3RiNDsm/RONiOtFb+HI0VXNU7wrW9c0mMITcjCYXrA8" +
        "5/+DhdAe6c3i/hZH7rhWF9RDnaXjHiurxDJNQeRHCUEqdijnbijLmAoDWu3ahqlB" +
        "aYX2A+Pqb3Y4E+Nk7cLctJo78xglLu8HUYmf541OyMAu1atvPEj/LC28c9H79zaC" +
        "3lVd4eVV8Z0VyALShg7l4FN1tlEMQCUeMHSCm3jjcqhVgtxqhNF2qFJq/sRvbHUC" +
        "t+1wEiBPlhg1Pfxum9AgyC58UG4OeWBrgrjWjA==");
    entity2.setSsoURL("https://idp.enea.it/idp/profile/SAML2/POST/SSO");
    entity2.setNameIdFormat("urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified");
          entities.put(entity1.getAlias(), entity1);
    entities.put(entity2.getAlias(), entity2);
  }

  @Test
  public void parseWayfDocumentTest() {
    idpService.parseWayfDocument(document);
    for(EntityDescriptor e : idpService.listIdps()) {
      assertTrue(entities.containsKey(e.getAlias()));
      assertEquals(entities.get(e.getAlias()), e);
    }
  }
}
