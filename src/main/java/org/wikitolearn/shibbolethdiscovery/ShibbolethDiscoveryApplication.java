package org.wikitolearn.shibbolethdiscovery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Application main class.
 *
 * @author Alessandro Tundo
 */
@SpringBootApplication
public class ShibbolethDiscoveryApplication {

  public static void main(String[] args) {
    SpringApplication.run(ShibbolethDiscoveryApplication.class, args);
  }
}
