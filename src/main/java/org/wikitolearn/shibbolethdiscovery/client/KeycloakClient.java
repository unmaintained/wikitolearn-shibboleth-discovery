package org.wikitolearn.shibbolethdiscovery.client;

import java.util.List;

import javax.ws.rs.core.Response;

import org.keycloak.admin.client.Keycloak;
import org.keycloak.representations.idm.IdentityProviderMapperRepresentation;
import org.keycloak.representations.idm.IdentityProviderRepresentation;
import org.keycloak.representations.idm.KeysMetadataRepresentation.KeyMetadataRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.wikitolearn.shibbolethdiscovery.config.ApplicationProperties;

/**
 * Service class that acts as a client.
 * Its goal is to interact with a Keycloak instance through its API.
 *
 * @author Alessandro Tundo
 */
@Service
public class KeycloakClient {
  private final Logger log = LoggerFactory.getLogger(KeycloakClient.class);
  private final ApplicationProperties applicationProperties;
  private final Keycloak keycloak;

  /**
   * Public constructor which initialize the Keycloak object needed to
   * perform API operations.
   * @param applicationProperties the application properties
   */
  public KeycloakClient(ApplicationProperties applicationProperties) {
    this.applicationProperties = applicationProperties;
    this.keycloak = Keycloak.getInstance(this.applicationProperties.getKeycloak().getUrl(),
        this.applicationProperties.getKeycloak().getMasterRealm(),
        this.applicationProperties.getKeycloak().getUsername(),
        this.applicationProperties.getKeycloak().getPassword(),
        this.applicationProperties.getKeycloak().getClient());
  }

  /**
   * List the current identity providers.
   * @return List the identity providers
   * @throw RuntimeException
   */
  public List<IdentityProviderRepresentation> listIdps() {
    try {
      return this.keycloak.realm(this.applicationProperties.getKeycloak().getRealm()).identityProviders()
          .findAll();
    } catch (Exception e) {
      log.error("Unable to list identity providers", e);
      throw new RuntimeException(e);
    }
  }

  /**
   * Get an identity provider given its alias.
   * @param alias the identity provider alias
   * @return IdentityProviderRepresentation the identity provider
   * @throws RuntimeException
   */
  public IdentityProviderRepresentation getIdp(String alias) {
    try {
      return this.keycloak.realm(this.applicationProperties.getKeycloak().getRealm()).identityProviders()
          .get(alias).toRepresentation();
    } catch (Exception e) {
      log.error("Unable to get the identity provider", e);
      throw new RuntimeException(e);
    }
  }

  /**
   * Create a new identity provider given its representation.
   * @param identityProvider the identity provider representation
   * @return Nothing.
   * @throws RuntimeException
   */
  public void createIdp(IdentityProviderRepresentation identityProvider) {
    try {
      Response response = this.keycloak.realm(this.applicationProperties.getKeycloak().getRealm()).identityProviders()
          .create(identityProvider);
      if (HttpStatus.CONFLICT.value() == response.getStatus()) {
        log.warn("Identity provider with alias {} already present", identityProvider.getAlias());
      }
    } catch (Exception e) {
      log.error("Unable to create identity provider", e);
      throw new RuntimeException(e);
    }
  }

  /**
   * Update an identity provider given its representation.
   * @param identityProvider the identity provider
   * @return Nothing.
   * @throws RuntimeException
   */
  public void updateIdp(IdentityProviderRepresentation identityProvider) {
    try {
      this.keycloak.realm(this.applicationProperties.getKeycloak().getRealm()).identityProviders()
          .get(identityProvider.getAlias()).update(identityProvider);
    } catch (Exception e) {
      log.error("Unable to update identity provider", e);
      throw new RuntimeException(e);
    }
  }

  /**
   * Delete an identity provider given its representation.
   * @param identityProvider the identity provider representation
   * @return Nothing.
   * @throws RuntimeException
   */
  public void deleteIdp(IdentityProviderRepresentation identityProvider) {
    try {
      this.keycloak.realm(this.applicationProperties.getKeycloak().getRealm()).identityProviders()
          .get(identityProvider.getAlias()).remove();
    } catch (Exception e) {
      log.error("Unable to delete identity provider", e);
      throw new RuntimeException(e);
    }
  }
      /**
   * Create an identity provider mapper for the given identity provider.
   * @param mapper the identity provider mapper representation
   * @param identityProvider the identity provider representation
   * @return Nothing.
   * @throws RuntimeException
   */
  public void createMapper(IdentityProviderMapperRepresentation mapper, IdentityProviderRepresentation identityProvider) {
    try {
      this.keycloak.realm(this.applicationProperties.getKeycloak().getRealm()).identityProviders()
      .get(identityProvider.getAlias()).addMapper(mapper);
    } catch(Exception e) {
      log.error("Unable to create mapper", e);
      throw new RuntimeException(e);
    }
  }

  /**
   * Retrieve the RSA key of the realm
   * @return KeyMetatadaRepresentation the RSA key of the realm
   */
  public KeyMetadataRepresentation retrieveRSAKey() {
    List<KeyMetadataRepresentation> keys = this.keycloak.realm(this.applicationProperties.getKeycloak().getRealm())
        .keys().getKeyMetadata().getKeys();
    KeyMetadataRepresentation rsaKey = null;
    for (KeyMetadataRepresentation key : keys) {
      if ("RSA".equals(key.getType())) {
        rsaKey = key;
      }
    }
    return rsaKey;
  }
}
