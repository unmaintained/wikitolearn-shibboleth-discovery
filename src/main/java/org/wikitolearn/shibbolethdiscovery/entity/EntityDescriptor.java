package org.wikitolearn.shibbolethdiscovery.entity;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.keycloak.representations.idm.IdentityProviderRepresentation;
import org.springframework.data.annotation.Id;
import org.springframework.data.keyvalue.annotation.KeySpace;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Identity provider POJO class.
 * This class is used as bridge between the WAYF file EntityDescriptor representation
 * and Keycloak IdentityProviderRepresentation.
 * Moreover it is used as representation format of identity providers for the
 * REST API exposed by this service.
 *
 * @author Alessandro Tundo
 */
@KeySpace("entities")
public class EntityDescriptor {

  @Id
  private String entityId;
  private String alias;
  private String name;
  private String description;
  private String logo;
  private String certificate;
  private Date registrationDate;
  private String ssoURL;
  private String logoutURL;
  private String nameIdFormat;

  /**
   * Public empty constructor.
   */
  public EntityDescriptor() {
  }

  /**
   * @return the entityId
   */
  public String getEntityId() {
    return entityId;
  }

  /**
   * @param entityId the entityId to set
   */
  public void setEntityId(String entityId) {
    this.entityId = entityId;
  }

  /**
   * @return the alias
   */
  public String getAlias() {
    return alias;
  }

  /**
   * @param alias the alias to set
   */
  public void setAlias(String alias) {
    this.alias = alias;
  }

  /**
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the description
   */
  public String getDescription() {
    return description;
  }

  /**
   * @param description the description to set
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * @return the logo
   */
  public String getLogo() {
    return logo;
  }

  /**
   * @param logo the logo to set
   */
  public void setLogo(String logo) {
    this.logo = logo;
  }

  /**
   * @return the certificate
   */
  @JsonIgnore
  public String getCertificate() {
    return certificate;
  }

  /**
   * @param certificate the certificate to set
   */
  public void setCertificate(String certificate) {
    this.certificate = certificate;
  }

  /**
   * @return the registrationDate
   */
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
  public Date getRegistrationDate() {
    return registrationDate;
  }

  /**
   * @param registrationDate the registrationDate to set
   */
  public void setRegistrationDate(Date registrationDate) {
    this.registrationDate = registrationDate;
  }

  /**
   * @return the ssoURL
   */
  public String getSsoURL() {
    return ssoURL;
  }

  /**
   * @param ssoURL the ssoURL to set
   */
  public void setSsoURL(String ssoURL) {
    this.ssoURL = ssoURL;
  }

  /**
   * @return the nameIdFormat
   */
  public String getNameIdFormat() {
    return nameIdFormat;
  }

  /**
   * @param nameIdFormat the nameIdFormat to set
   */
  public void setNameIdFormat(String nameIdFormat) {
    this.nameIdFormat = nameIdFormat;
  }

  /**
   * @return the logoutURL
   */
  public String getLogoutURL() {
    return logoutURL;
  }

  /**
   * @param logoutURL the logoutURL to set
   */
  public void setLogoutURL(String logoutURL) {
    this.logoutURL = logoutURL;
  }

  public IdentityProviderRepresentation toIdentityProviderRepresentation() {
    IdentityProviderRepresentation identityProvider = new IdentityProviderRepresentation();
    identityProvider.setAlias(alias);
    identityProvider.setProviderId("saml");
    identityProvider.setEnabled(true);
    identityProvider.setTrustEmail(false);
    identityProvider.setStoreToken(false);
    identityProvider.setAddReadTokenRoleOnCreate(false);
    identityProvider.setDisplayName(name);
    Map<String, String> config = new HashMap<String, String>();
    config.put("nameIDPolicyFormat", nameIdFormat);
    config.put("singleSignOnServiceUrl", ssoURL);
    config.put("singleLogoutServiceUrl", logoutURL);
    config.put("backchannelSupported", "true");
    config.put("validateSignature", "false");
    config.put("signingCertificate", certificate);
    if (logoutURL != null && !"".equals(logoutURL)) {
      config.put("postBindingLogout", "true");
    }
    config.put("postBindingResponse", "true");
    config.put("postBindingAuthnRequest", "true");
    config.put("forceAuthn", "false");
    config.put("wantAuthnRequestsSigned", "false");
    identityProvider.setConfig(config);
    return identityProvider;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("EntityDescriptor [entityId=").append(entityId).append(", alias=").append(alias)
        .append(", name=").append(name).append(", description=").append(description).append(", logo=")
        .append(logo).append(", certificate=").append(certificate).append(", registrationDate=")
        .append(registrationDate).append(", ssoURL=").append(ssoURL).append(", logoutURL=").append(logoutURL)
        .append(", nameIdFormat=").append(nameIdFormat).append("]");
    return builder.toString();
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((alias == null) ? 0 : alias.hashCode());
    result = prime * result + ((certificate == null) ? 0 : certificate.hashCode());
    result = prime * result + ((description == null) ? 0 : description.hashCode());
    result = prime * result + ((entityId == null) ? 0 : entityId.hashCode());
    result = prime * result + ((logo == null) ? 0 : logo.hashCode());
    result = prime * result + ((logoutURL == null) ? 0 : logoutURL.hashCode());
    result = prime * result + ((name == null) ? 0 : name.hashCode());
    result = prime * result + ((nameIdFormat == null) ? 0 : nameIdFormat.hashCode());
    result = prime * result + ((registrationDate == null) ? 0 : registrationDate.hashCode());
    result = prime * result + ((ssoURL == null) ? 0 : ssoURL.hashCode());
    return result;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (!(obj instanceof EntityDescriptor)) {
      return false;
    }
    EntityDescriptor other = (EntityDescriptor) obj;
    if (alias == null) {
      if (other.alias != null) {
        return false;
      }
    } else if (!alias.equals(other.alias)) {
      return false;
    }
    if (certificate == null) {
      if (other.certificate != null) {
        return false;
      }
    } else if (!certificate.equals(other.certificate)) {
      return false;
    }
    if (description == null) {
      if (other.description != null) {
        return false;
      }
    } else if (!description.equals(other.description)) {
      return false;
    }
    if (entityId == null) {
      if (other.entityId != null) {
        return false;
      }
    } else if (!entityId.equals(other.entityId)) {
      return false;
    }
    if (logo == null) {
      if (other.logo != null) {
        return false;
      }
    } else if (!logo.equals(other.logo)) {
      return false;
    }
    if (logoutURL == null) {
      if (other.logoutURL != null) {
        return false;
      }
    } else if (!logoutURL.equals(other.logoutURL)) {
      return false;
    }
    if (name == null) {
      if (other.name != null) {
        return false;
      }
    } else if (!name.equals(other.name)) {
      return false;
    }
    if (nameIdFormat == null) {
      if (other.nameIdFormat != null) {
        return false;
      }
    } else if (!nameIdFormat.equals(other.nameIdFormat)) {
      return false;
    }
    if (registrationDate == null) {
      if (other.registrationDate != null) {
        return false;
      }
    } else if (!registrationDate.equals(other.registrationDate)) {
      return false;
    }
    if (ssoURL == null) {
      if (other.ssoURL != null) {
        return false;
      }
    } else if (!ssoURL.equals(other.ssoURL)) {
      return false;
    }
    return true;
  }
}
