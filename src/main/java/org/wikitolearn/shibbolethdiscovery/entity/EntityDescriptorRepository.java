package org.wikitolearn.shibbolethdiscovery.entity;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

/**
 * CrudRepository for the EntityDescriptor.
 *
 * @author Alessandro Tundo
 */
public interface EntityDescriptorRepository extends CrudRepository<EntityDescriptor, String> {
  EntityDescriptor findByAlias(String alias);
  List<EntityDescriptor> findAll();
}
