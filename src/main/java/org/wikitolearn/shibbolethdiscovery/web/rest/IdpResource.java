package org.wikitolearn.shibbolethdiscovery.web.rest;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.wikitolearn.shibbolethdiscovery.entity.EntityDescriptor;
import org.wikitolearn.shibbolethdiscovery.service.IdpService;

/**
 * REST controller that exposes the identity provider resource.
 *
 * @author Alessandro Tundo
 */
@RestController
@RequestMapping(value = "/idps")
public class IdpResource {
  @Autowired
  private IdpService idpService;

  /**
   * List identity providers.
   * @return Collection JSON serialization of identity providers
   */
  @RequestMapping(method = RequestMethod.GET)
  public Collection<EntityDescriptor> getIdps() {
    return idpService.listIdps();
  }
}
