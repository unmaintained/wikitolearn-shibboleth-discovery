package org.wikitolearn.shibbolethdiscovery.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.map.repository.config.EnableMapRepositories;

/**
 * Configure and enable scan for MapRepositories.
 *
 * @author Alessandro Tundo
 */
@Configuration
@EnableMapRepositories(basePackages = "org.wikitolearn.shibbolethdiscovery.entity")
public class RepositoryConfiguration {

}
