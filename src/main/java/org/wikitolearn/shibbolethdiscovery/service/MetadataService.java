package org.wikitolearn.shibbolethdiscovery.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wikitolearn.shibbolethdiscovery.client.KeycloakClient;
import org.wikitolearn.shibbolethdiscovery.config.ApplicationProperties;
import org.wikitolearn.shibbolethdiscovery.entity.EntityDescriptor;

/**
 * Service class that encapsulates the logic needed to expose valid Shibboleth
 * Service Provider metadata.
 *
 * @author Alessandro Tundo
 */
@Service
public class MetadataService {

  @Autowired
  private ApplicationProperties applicationProperties;

  @Autowired
  private IdpService idpService;

  @Autowired
  private KeycloakClient keycloakClient;

  /**
   * Construct valid Shibboleth Service Provider metadata containing Service Provider info
   * identity providers consuming assertions.
   * @return String the service provider XML metadata
   */
  public String getMetadata() {
    StringBuffer xml = new StringBuffer();
    xml.append(
        "<md:EntityDescriptor xmlns:md=\"urn:oasis:names:tc:SAML:2.0:metadata\" ID=\"_666acb6d4439006afef16dfd1a338b77\" entityID=\""
            + this.applicationProperties.getKeycloak().getPublicUrl() + "/realms/"
            + this.applicationProperties.getKeycloak().getRealm() + "\">");
    xml.append(
        "<md:SPSSODescriptor protocolSupportEnumeration=\"urn:oasis:names:tc:SAML:2.0:protocol urn:oasis:names:tc:SAML:1.1:protocol\">");
          xml.append("<md:Extensions>");
    xml.append("<mdui:UIInfo xmlns:mdui=\"urn:oasis:names:tc:SAML:metadata:ui\">");
    xml.append("<mdui:DisplayName xml:lang=\"en\">" + this.applicationProperties.getSp().getUiDisplayNameEn()
        + "</mdui:DisplayName>");
    xml.append("<mdui:DisplayName xml:lang=\"it\">" + this.applicationProperties.getSp().getUiDisplayName()
        + "</mdui:DisplayName>");
    xml.append("<mdui:Description xml:lang=\"en\">" + this.applicationProperties.getSp().getDescriptionEn()
        + "</mdui:Description>");
    xml.append("<mdui:Description xml:lang=\"it\">" + this.applicationProperties.getSp().getDescription()
        + "</mdui:Description>");
    xml.append("<mdui:InformationURL xml:lang=\"en\">" + this.applicationProperties.getSp().getInfoUrlEn()
        + "</mdui:InformationURL>");
    xml.append("<mdui:InformationURL xml:lang=\"it\">" + this.applicationProperties.getSp().getInfoUrl()
        + "</mdui:InformationURL>");
    xml.append("<mdui:PrivacyStatementURL xml:lang=\"en\">" + this.applicationProperties.getSp().getPrivacyUrlEn()
        + "</mdui:PrivacyStatementURL>");
    xml.append("<mdui:PrivacyStatementURL xml:lang=\"it\">" + this.applicationProperties.getSp().getPrivacyUrl()
        + "</mdui:PrivacyStatementURL>");
    xml.append("<mdui:Logo height=\"16\" width=\"16\" xml:lang=\"en\">"
        + this.applicationProperties.getSp().getLogoUrlEn() + "</mdui:Logo>");
    xml.append("<mdui:Logo height=\"16\" width=\"16\" xml:lang=\"it\">"
        + this.applicationProperties.getSp().getLogoUrl() + "</mdui:Logo>");
    xml.append("</mdui:UIInfo>");
    xml.append("</md:Extensions>");
          xml.append("<md:KeyDescriptor>");
    xml.append("<ds:KeyInfo xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\">");
    xml.append("<ds:KeyName>" + this.keycloakClient.retrieveRSAKey().getKid() + "</ds:KeyName>");
    xml.append("<ds:X509Data>");
    xml.append("<ds:X509Certificate>" + this.keycloakClient.retrieveRSAKey().getCertificate()
        + "</ds:X509Certificate>");
    xml.append("</ds:X509Data>");
    xml.append("</ds:KeyInfo>");
    xml.append("</md:KeyDescriptor>");
          xml.append("<md:NameIDFormat>urn:oasis:names:tc:SAML:2.0:nameid-format:persistent");
    xml.append("</md:NameIDFormat>");

    int idpIndex = 0;
    for (EntityDescriptor descriptor : idpService.listIdps()) {
      idpIndex++;
      xml.append(
          "<md:AssertionConsumerService Binding=\"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST\" Location=\"")
          .append(this.applicationProperties.getKeycloak().getPublicUrl())
          .append("/realms/" + this.applicationProperties.getKeycloak().getRealm() + "/broker/")
          .append(descriptor.getAlias()).append("/endpoint\" index=\"").append(idpIndex).append("\"/>");
    }
          xml.append("<md:AttributeConsumingService index=\"1\">");
    xml.append("<md:ServiceName xml:lang=\"en\">" + this.applicationProperties.getSp().getUiDisplayNameEn() + "</md:ServiceName>");
    xml.append("<md:ServiceName xml:lang=\"it\">" + this.applicationProperties.getSp().getUiDisplayName() + "</md:ServiceName>");
    xml.append("<md:ServiceDescription xml:lang=\"en\">" + this.applicationProperties.getSp().getDescriptionEn() + "</md:ServiceDescription>");
    xml.append("<md:ServiceDescription xml:lang=\"it\">" + this.applicationProperties.getSp().getDescription() + "</md:ServiceDescription>");
    xml.append("<md:RequestedAttribute FriendlyName=\"email\" Name=\"urn:oid:0.9.2342.19200300.100.1.3\" NameFormat=\"urn:oasis:names:tc:SAML:2.0:attrname-format:uri\" isRequired=\"true\"/>"
        + "<md:RequestedAttribute FriendlyName=\"eduPersonPrincipalName\" Name=\"urn:oid:1.3.6.1.4.1.5923.1.1.1.6\" NameFormat=\"urn:oasis:names:tc:SAML:2.0:attrname-format:uri\" isRequired=\"true\"/>"
        + "<md:RequestedAttribute FriendlyName=\"eduPersonTargetedID\" Name=\"urn:oid:1.3.6.1.4.1.5923.1.1.1.10\" NameFormat=\"urn:oasis:names:tc:SAML:2.0:attrname-format:uri\" isRequired=\"true\"/>"
        + "<md:RequestedAttribute FriendlyName=\"givenName\" Name=\"urn:oid:2.5.4.42\" NameFormat=\"urn:oasis:names:tc:SAML:2.0:attrname-format:uri\" isRequired=\"true\"/>"
        + "<md:RequestedAttribute FriendlyName=\"surname\" Name=\"urn:oid:2.5.4.4\" NameFormat=\"urn:oasis:names:tc:SAML:2.0:attrname-format:uri\" isRequired=\"true\"/>");
    xml.append("</md:AttributeConsumingService>");
          xml.append("</md:SPSSODescriptor>");
          xml.append("<md:Organization>");
    xml.append("<md:OrganizationName xml:lang=\"en\">" + this.applicationProperties.getSp().getOrgNameEn()
        + "</md:OrganizationName>");
    xml.append("<md:OrganizationName xml:lang=\"it\">" + this.applicationProperties.getSp().getOrgName()
        + "</md:OrganizationName>");
    xml.append("<md:OrganizationDisplayName xml:lang=\"en\">"
        + this.applicationProperties.getSp().getOrgDisplayNameEn() + "</md:OrganizationDisplayName>");
    xml.append("<md:OrganizationDisplayName xml:lang=\"it\">"
        + this.applicationProperties.getSp().getOrgDisplayName() + "</md:OrganizationDisplayName>");
    xml.append("<md:OrganizationURL xml:lang=\"en\">" + this.applicationProperties.getSp().getOrgUrlEn()
        + "</md:OrganizationURL>");
    xml.append("<md:OrganizationURL xml:lang=\"it\">" + this.applicationProperties.getSp().getOrgUrl()
        + "</md:OrganizationURL>");
    xml.append("</md:Organization>");
          xml.append("<md:ContactPerson contactType=\"technical\">");
    xml.append("<md:GivenName>" + this.applicationProperties.getSp().getContactName() + "</md:GivenName>");
    xml.append("<md:SurName>" + this.applicationProperties.getSp().getContactSurname() + "</md:SurName>");
    xml.append("<md:EmailAddress>mailto:" + this.applicationProperties.getSp().getContactEmail()
        + "</md:EmailAddress>");
    xml.append("</md:ContactPerson>");
          xml.append("</md:EntityDescriptor>");
    return xml.toString();
  }
}
