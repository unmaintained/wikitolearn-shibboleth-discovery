package org.wikitolearn.shibbolethdiscovery.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.keycloak.representations.idm.IdentityProviderMapperRepresentation;
import org.keycloak.representations.idm.IdentityProviderRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.wikitolearn.shibbolethdiscovery.client.FederationWayfClient;
import org.wikitolearn.shibbolethdiscovery.client.KeycloakClient;
import org.wikitolearn.shibbolethdiscovery.config.ApplicationProperties;
import org.wikitolearn.shibbolethdiscovery.entity.EntityDescriptor;
import org.wikitolearn.shibbolethdiscovery.entity.EntityDescriptorRepository;

/**
 * Service class that encapsulate the logic needed for synchronize the identity providers
 * of the Federation.
 *
 * @author Alessandro Tundo
 */
@Service
public class IdpService {
  private final Logger log = LoggerFactory.getLogger(IdpService.class);

  @Autowired
  private FederationWayfClient federationClient;

  @Autowired
  private KeycloakClient keycloakClient;

  @Autowired
  private ApplicationProperties applicationProperties;

  @Autowired
  private EntityDescriptorRepository entityRepository;

  private SimpleDateFormat sdf;

  /**
   * Public constructor.
   */
  public IdpService() {
    this.sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
  }

  /**
   * List the identity providers extracted from the Federation WAYF file during last
   * synchronization process.
   * @return List the identity providers
   */
  public List<EntityDescriptor> listIdps() {
    return this.entityRepository.findAll();
  }
  /**
   * List the identity provider aliases extracted from the Federation WAYF file during last
   * synchronization process.
   * @return Set the identity provider aliases
   */
  public Set<String> listIdpsAliases() {
    return this.entityRepository.findAll().stream().map(entity -> entity.getAlias()).collect(Collectors.toSet());
  }
  /**
   * List the identity provider as a <code>Map<String, EntityDescriptor></code> extracted from the Federation WAYF file during last
   * synchronization process.
   * @return Map the identity provider map
   */
  public Map<String, EntityDescriptor> listIdpsAsMap() {
    return this.entityRepository.findAll().stream().collect(Collectors.toMap(entity -> entity.getAlias(), entity -> entity));
  }

  /**
   * Scheduled identity provider synchronization.
   * The method is executed at a fixed rate configured by application properties.
   * It compares current available Keycloak identity providers with those extracted
   * from the Federation WAYF file. Then performs additions, deletions and updates
   * exploting the KeycloakClient.
   * @return Nothing.
   */
  @Scheduled(fixedRateString = "${application.syncRate}", initialDelay = 0)
  public void synchronizeIdps() {
    try {
      List<IdentityProviderRepresentation> keycloakIdps = this.keycloakClient.listIdps();
      parseWayfDocument(this.federationClient.downloadWayfFile());

      final Set<String> keycloakIdpAliases = keycloakIdps.stream().map(idp -> idp.getAlias())
          .collect(Collectors.toSet());

      List<IdentityProviderRepresentation> toBeDeleted = keycloakIdps.stream()
          .filter(isDeleted(this.listIdpsAliases(), this.applicationProperties.getIdp().getAliasPrefix()))
          .collect(Collectors.<IdentityProviderRepresentation>toList());
      List<IdentityProviderRepresentation> toBeUpdated = keycloakIdps.stream().filter(isOutdated(this.listIdpsAsMap()))
          .collect(Collectors.<IdentityProviderRepresentation>toList());
      List<EntityDescriptor> toBeAdded = this.listIdps().stream().filter(isNew(keycloakIdpAliases))
          .collect(Collectors.<EntityDescriptor>toList());
      List<IdentityProviderMapperRepresentation> mappers = buildMappers();

      for (IdentityProviderRepresentation idp : toBeDeleted) {
        this.keycloakClient.deleteIdp(idp);
      }
      log.info("{} identity providers deleted", toBeDeleted.size());

      for (IdentityProviderRepresentation idp : toBeUpdated) {
        this.keycloakClient.updateIdp(idp);
      }
      log.info("{} identity providers updated", toBeUpdated.size());

      for (EntityDescriptor entity : toBeAdded) {
        this.keycloakClient.createIdp(entity.toIdentityProviderRepresentation());
        for (IdentityProviderMapperRepresentation mapper : mappers) {
          mapper.setIdentityProviderAlias(entity.getAlias());
          this.keycloakClient.createMapper(mapper, entity.toIdentityProviderRepresentation());
        }
      }
      log.info("{} identity providers with mappers added", toBeAdded.size());

      log.info("Synchronization completed");
    } catch (Exception e) {
      log.error("Synchronization failed", e);
    }

  }

  /**
   * Parse and extract identity providers from the downloaded Federation WAYF file.
   * @param document the DOM representation of the Federation WAYF XML file
   * @return Nothing.
   */
  public void parseWayfDocument(Document document) {
    try {
      this.entityRepository.deleteAll();
      Element root = document.getDocumentElement();
      NodeList idps = root.getElementsByTagName("IDPSSODescriptor");
      for (int i = 0; i < idps.getLength(); i++) {
        EntityDescriptor entity = new EntityDescriptor();
        Node idpNode = idps.item(i);
        Element idpElement = (Element) idpNode;
        Node entityDescriptorNode = idpNode.getParentNode();
        Element entityElement = (Element) entityDescriptorNode;

        // Set entityId
        entity.setEntityId(entityDescriptorNode.getAttributes().getNamedItem("entityID").getTextContent());

        // Set registrationDate
        Node registrationInstant = entityElement.getElementsByTagName("mdrpi:RegistrationInfo").item(0)
            .getAttributes().getNamedItem("registrationInstant");
        if (registrationInstant != null) {
          entity.setRegistrationDate(this.sdf.parse(registrationInstant.getTextContent()));
        }

        // Set displayName
        String displayName = extractUiInfo(entity, idpElement, "mdui:DisplayName",
            this.applicationProperties.getPreferredLanguage());
        if (displayName != null) {
          entity.setName(displayName);
        }

        // Set description
        String description = extractUiInfo(entity, idpElement, "mdui:Description",
            this.applicationProperties.getPreferredLanguage());
        if (description != null) {
          entity.setDescription(description);
        }

        // Set logo
        String logo = extractUiInfo(entity, idpElement, "mdui:Logo",
            this.applicationProperties.getPreferredLanguage());
        if (logo != null) {
          entity.setLogo(logo.trim());
        }

        // Set certificate
        Node certificate = idpElement.getElementsByTagName("ds:X509Certificate").item(0);
        entity.setCertificate(certificate.getTextContent().trim().replaceAll("\\s+", ""));

        // Set alias
        entity.setAlias(applicationProperties.getIdp().getAliasPrefix()
            + DigestUtils.md5Hex(entity.getEntityId()).toUpperCase());

        // Set nameIdFormat
        NodeList nameIdFormats = idpElement.getElementsByTagName("NameIDFormat");
        String nameIdFormat = null;
        for (int j = 0; j < nameIdFormats.getLength(); j++) {
          Node nameIdFormatNode = nameIdFormats.item(j);
          if ("urn:oasis:names:tc:SAML:2.0:nameid-format:persistent"
              .equals(nameIdFormatNode.getTextContent())) {
            nameIdFormat = nameIdFormatNode.getTextContent();
            break;
          }
        }
        if (nameIdFormat != null) {
          entity.setNameIdFormat(nameIdFormat);
        } else {
          entity.setNameIdFormat("urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified");
        }

        // Set ssoURL
        String ssoURL = extractSsoUrl(entity, idpElement, "SingleSignOnService");
        entity.setSsoURL(ssoURL);

        // Set logoutURL
        String logoutURL = extractSsoUrl(entity, idpElement, "SingleLogoutService");
        entity.setLogoutURL(logoutURL);

        this.entityRepository.save(entity);
      }
    } catch (Exception e) {
      log.error("Unable to parse WAYF file", e);
    }
  }

  /**
   * Utility method to extract the SSO URL of an identity provider.
   * @param entity the identity provider in parsing
   * @param idpElement the element node
   * @param elementTag the XML node tag name
   * @return String the SSO URL extracted
   */
  private String extractSsoUrl(EntityDescriptor entity, Element idpElement, String elementTag) {
    NodeList ssoUrls = idpElement.getElementsByTagName(elementTag);
    String url = null;
    for (int i = 0; i < ssoUrls.getLength(); i++) {
      Node ssoUrl = ssoUrls.item(i);
      if ("urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST"
          .equals(ssoUrl.getAttributes().getNamedItem("Binding").getTextContent())) {
        url = ssoUrl.getAttributes().getNamedItem("Location").getTextContent();
      }
    }
    return url;
  }

  /**
   * Utility method to extract UI Info of an identity provider given a preferred language.
   * @param entity the identity provider in parsing
   * @param idpElement the element node
   * @param elementTag the XML node tag name
   * @param preferredLanguage the preferred language
   * @return String the UI info extracted
   */
  private String extractUiInfo(EntityDescriptor entity, Element idpElement, String elementTag, String preferredLanguage) {
    NodeList uiInfoNodes = idpElement.getElementsByTagName(elementTag);
    String uiInfo = null;

    for (int i = 0; i < uiInfoNodes.getLength(); i++) {
      Node langAttribute = uiInfoNodes.item(i).getAttributes().getNamedItem("xml:lang");
      if (null != langAttribute && preferredLanguage.equals(langAttribute.getTextContent())) {
        String sanitizedString = uiInfoNodes.item(i).getTextContent().replaceAll("[\\n\\r\\t]+", " ").trim();
        uiInfo = StringEscapeUtils.unescapeHtml(sanitizedString);
        break;
      }
    }
    return uiInfo;
  }

  /**
   * Utility method to build the identity provider mappers defined into application properties.
   * @return List a list of mappers
   */
  @SuppressWarnings("unchecked")
  private List<IdentityProviderMapperRepresentation> buildMappers() {
    List<IdentityProviderMapperRepresentation> mappers = new ArrayList<>();
    for(Map<String, Object> mapperProperties : this.applicationProperties.getIdp().getMappers()) {
      String name = (String) mapperProperties.get("name");
      String mapperType = (String) mapperProperties.get("mapperType");
      Map<String, String> configs = (Map<String, String>) mapperProperties.get("configs");
      mappers.add(buildMapper(name, mapperType, configs));
    }
    return mappers;
  }

  /**
   * Utility method to build a mapper given the name, the type and a map of configurations.
   * @param name the mapper name
   * @param mapperType the mapper type
   * @param configs the mapper configurations
   * @return IdentityProviderMapperRepresentation the mapper
   */
  private IdentityProviderMapperRepresentation buildMapper(String name, String mapperType, Map<String, String> configs) {
    IdentityProviderMapperRepresentation mapper = new IdentityProviderMapperRepresentation();
    mapper.setName(name);
    mapper.setIdentityProviderMapper(mapperType);
    mapper.setConfig(configs);
    return mapper;
  }

  /**
   * Predicate to determine if an identity provider has been deleted.
   * @param aliases a set of identity provider aliases
   * @param aliasPrefix the identity provider alias prefix
   * @return true if the identity provider needs be deleted
   */
  private static Predicate<IdentityProviderRepresentation> isDeleted(Set<String> aliases, String aliasPrefix) {
    return p -> !aliases.contains(p.getAlias()) && p.getAlias().startsWith(aliasPrefix);
  }

  /**
   * Predicate to determine if an identity provider is outdated.
   * @param entities a map of idenity providers
   * @return true if the identity provider is outdated
   */
  private static Predicate<IdentityProviderRepresentation> isOutdated(Map<String, EntityDescriptor> entities) {
    return p -> entities.get(p.getAlias()) != null
        && !p.getConfig().entrySet().equals(entities.get(p.getAlias()).toIdentityProviderRepresentation().getConfig().entrySet());
  }

  /**
   * Predicate to determine if an identity provider alias is new.
   * @param aliases a set of identity provider aliases
   * @return true if the alias is new
   */
  private static Predicate<EntityDescriptor> isNew(Set<String> aliases) {
    return p -> !aliases.contains(p.getAlias());
  }
}
